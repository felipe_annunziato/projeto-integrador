package controller;
import java.util.List;

import dao.EmpresaDAO;
import model.Empresa;

public class EmpresaController {
	
	public void cadastrar(Empresa empresa) {
		new EmpresaDAO().cadastrar(empresa);
	}
	
	public void editar(Empresa empresa) {
		new EmpresaDAO().editar(empresa);
	}
	
	public List<Empresa> buscarTodos() {
		return new EmpresaDAO().buscarTodos();
	}
	
	public void excluir(Integer id) {
		new EmpresaDAO().excluir(id);
	}
}
