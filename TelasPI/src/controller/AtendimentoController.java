package controller;

import java.util.List;

import dao.AtendimentoDAO;
import model.Atendimento;
import model.Empresa;
import model.UserCliente;
import model.Vaga;

public class AtendimentoController {
	
	public List<Empresa> listaEmpresa() {
		return new AtendimentoDAO().listaEmpresa();
	}
	
	public void iniciaAtendimento(Empresa empresa, UserCliente cliente, Atendimento atendimento) {
		new AtendimentoDAO().iniciaAtendimento(empresa, cliente, atendimento);
	}
	
	public List<Atendimento> listaAtendimentosAbertos(){
		return new AtendimentoDAO().listaAtendimentosAbertos();
	}
	
	public List<Atendimento> listaAtendimentosFechados(){
		return new AtendimentoDAO().listaAtendimentosFechados();
	}
	
	public void finalizaAtendimento(Atendimento atendimento) {
		new AtendimentoDAO().finalizaAtendimento(atendimento);
	}
	
	public void atualizaStatusVagaFree(Vaga vaga) {
		new AtendimentoDAO().atualizaStatusVagaFree(vaga);
	}
	
	public void atualizaStatusVagaBusy(Vaga vaga) {
		new AtendimentoDAO().atualizaStatusVagaBusy(vaga);
	}

//	public void calculaValor(Atendimento atendimento) {
//		new AtendimentoDAO().calculaValor(atendimento);
//	}
}
