package controller;

import java.util.List;

import dao.VagaDAO;
import model.Empresa;
import model.Vaga;

public class VagaController {
	
	public List<Vaga> listarVagas(){
		return new VagaDAO().listarVagas();
		
	}
	
	public void cadastraVagas(Vaga vaga) {
		new VagaDAO().cadastraVagas(vaga);
	}
	
	public List<Vaga> listarVagasLivres(Empresa empresa) {
		return new VagaDAO().listarVagasLivres(empresa);
	}
	
	public void editar(Vaga vaga) {
		new VagaDAO().editar(vaga);
	}

}
