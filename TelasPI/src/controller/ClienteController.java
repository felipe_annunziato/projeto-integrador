package controller;

import java.util.List;

import dao.ClienteDAO;
import model.UserCliente;

public class ClienteController {
	
	public void salvar(UserCliente cliente) {
		new ClienteDAO().salvar(cliente);
	}
	
	public void editar(UserCliente cliente) {
		new ClienteDAO().editar(cliente);
	}
	
	public List<UserCliente> listarClientes(){
		return new ClienteDAO().listarClientes();
	}
	
	public void excluir(Integer id) {
		new ClienteDAO().excluir(id);
	}
}
