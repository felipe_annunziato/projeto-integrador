package view;

import java.awt.Color;
import java.awt.EventQueue;

import javax.swing.JInternalFrame;
import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JPanel;
import javax.swing.JButton;
import javax.swing.LayoutStyle.ComponentPlacement;

import controller.AtendimentoController;
import controller.VagaController;
import model.Empresa;
import model.Vaga;

import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.event.ActionListener;
import java.nio.DoubleBuffer;
import java.awt.event.ActionEvent;

public class EditarVagasUI extends JInternalFrame {
	private JTextField txtValorDiaria;
	private AtendimentoController atendimentoController = new AtendimentoController();
	private VagaController vagaController = new VagaController();
	private Vaga vaga;
	private JTextField txtValorHora;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					EditarVagasUI frame = new EditarVagasUI(null);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public EditarVagasUI(Vaga vagaParaEditar) {
		this.vaga = vagaParaEditar;
		setClosable(true);
		setTitle("Editar Vaga");
		setBounds(100, 100, 450, 300);
		
		JPanel panel = new JPanel();
		
		JCheckBox chckbxFree = new JCheckBox("Free");
		JCheckBox chckbxBusy = new JCheckBox("Busy");
		
		chckbxBusy.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				chckbxFree.setSelected(false);
			}
		});
		
		chckbxFree.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				chckbxBusy.setSelected(false);
			}
		});
		
		JButton btnEditar = new JButton("Editar");
		btnEditar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {			
				if(chckbxFree.isSelected()) {
					vaga.setStatus("Free");
				} else if(chckbxBusy.isSelected()) {
					vaga.setStatus("Busy");
				}
				vaga.setValor_diaria((Double.parseDouble(txtValorDiaria.getText())));
				vaga.setValor_hora(Double.parseDouble(txtValorHora.getText()));
				vagaController.editar(vaga);
				JOptionPane.showMessageDialog(null, "Vaga editada com sucesso!");
				dispose();
				
				}catch (Exception e1) {
					System.out.println(e1.getMessage());
				}	
			
			}
		}	
		);
		
		JButton btnCancelar = new JButton("Cancelar");
		btnCancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		GroupLayout groupLayout = new GroupLayout(getContentPane());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(panel, GroupLayout.PREFERRED_SIZE, 416, Short.MAX_VALUE)
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(btnEditar)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(btnCancelar)))
					.addContainerGap())
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addComponent(panel, GroupLayout.PREFERRED_SIZE, 199, GroupLayout.PREFERRED_SIZE)
					.addGap(18)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(btnEditar)
						.addComponent(btnCancelar))
					.addContainerGap(14, Short.MAX_VALUE))
		);
			
		
		txtValorDiaria = new JTextField();
		txtValorDiaria.setColumns(10);
		

		
		JLabel lblValor = new JLabel("Valor Diária");
		
		JLabel lblStatus = new JLabel("Status");
		
		txtValorHora = new JTextField();
		txtValorHora.setColumns(10);
		
		JLabel lblValorHora = new JLabel("Valor Hora");
		GroupLayout gl_panel = new GroupLayout(panel);
		gl_panel.setHorizontalGroup(
			gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel.createSequentialGroup()
					.addGap(24)
					.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
						.addComponent(lblStatus)
						.addGroup(gl_panel.createSequentialGroup()
							.addComponent(chckbxFree)
							.addGap(30)
							.addComponent(chckbxBusy))
						.addGroup(gl_panel.createSequentialGroup()
							.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
								.addComponent(lblValorHora, GroupLayout.PREFERRED_SIZE, 87, GroupLayout.PREFERRED_SIZE)
								.addComponent(txtValorHora, GroupLayout.PREFERRED_SIZE, 114, GroupLayout.PREFERRED_SIZE))
							.addGap(27)
							.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
								.addComponent(txtValorDiaria, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(lblValor))))
					.addContainerGap(137, Short.MAX_VALUE))
		);
		gl_panel.setVerticalGroup(
			gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblValorHora)
						.addComponent(lblValor))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
						.addComponent(txtValorHora, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(txtValorDiaria, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(29)
					.addComponent(lblStatus)
					.addGap(18)
					.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
						.addComponent(chckbxFree)
						.addComponent(chckbxBusy))
					.addContainerGap(63, Short.MAX_VALUE))
		);
		panel.setLayout(gl_panel);
		getContentPane().setLayout(groupLayout);
		
		if(vagaParaEditar != null) {
			txtValorDiaria.setText(Double.toString(vagaParaEditar.getValor_diaria()));
			txtValorHora.setText(Double.toString(vagaParaEditar.getValor_hora()));
			
		}
		
	}
}
