package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JMenuBar;
import javax.swing.JTextArea;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.util.List;
import java.awt.event.ActionEvent;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.event.MenuListener;

import dao.AtendimentoDAO;
import model.Empresa;

import javax.swing.event.MenuEvent;

public class PrincipalUI extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					PrincipalUI frame = new PrincipalUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public PrincipalUI() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenu menuCadastro = new JMenu("Cadastro");
		menuBar.add(menuCadastro);
		
		JMenuItem mntmCliente = new JMenuItem("Cliente");
		mntmCliente.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				CadastroClienteUI cadastroCliente = new CadastroClienteUI(null);
				cadastroCliente.setVisible(true);
				contentPane.add(cadastroCliente,0);
			}
		});
		menuCadastro.add(mntmCliente);
		
		JMenuItem mntmEmpresa = new JMenuItem("Empresa");
		mntmEmpresa.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				CadastroEmpresaUI cadastroEmpresaUI = new CadastroEmpresaUI(null);
				cadastroEmpresaUI.setVisible(true);
				contentPane.add(cadastroEmpresaUI);
			}
		});
		menuCadastro.add(mntmEmpresa);
		
		JMenuItem mntmVagas_1 = new JMenuItem("Vagas");
		mntmVagas_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				CadastroVagasUI cVagas = new CadastroVagasUI();
				cVagas.setVisible(true);
				contentPane.add(cVagas,0);
			}
		});
		menuCadastro.add(mntmVagas_1);
		
		JMenu menuConsulta = new JMenu("Consulta");
		menuBar.add(menuConsulta);
		
		JMenuItem mntmCliente_1 = new JMenuItem("Cliente");
		mntmCliente_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ConsultaClienteUI consulCliente = new ConsultaClienteUI();
				consulCliente.setVisible(true);
				contentPane.add(consulCliente,0);
			}
		});
		menuConsulta.add(mntmCliente_1);
		
		JMenuItem mntmEmpresa_1 = new JMenuItem("Empresa");
		mntmEmpresa_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ConsultaEmpresaUI consulEmpresa = new ConsultaEmpresaUI();
				consulEmpresa.setVisible(true);
				contentPane.add(consulEmpresa, 0);
			}
		});
		menuConsulta.add(mntmEmpresa_1);
		
		JMenuItem mntmVagas = new JMenuItem("Vagas");
		mntmVagas.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				ConsultaVagasUI consulVaga = new ConsultaVagasUI();
				consulVaga.setVisible(true);
				contentPane.add(consulVaga, 0);
			}
		});
		menuConsulta.add(mntmVagas);
		
		JMenu mnReservaVaga = new JMenu("Atendimento");
		menuBar.add(mnReservaVaga);
		
		JMenuItem mntmIniciarAtendimento = new JMenuItem("Iniciar Atendimento");
		mntmIniciarAtendimento.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				CadastroAtendimentoUI cAtendimentoUI = new CadastroAtendimentoUI();
				cAtendimentoUI.setVisible(true);
				contentPane.add(cAtendimentoUI,0);
			}
		});
		mnReservaVaga.add(mntmIniciarAtendimento);
		
		JMenuItem mntmFinalizarAtendimento = new JMenuItem("Atendimentos em andamento");
		mntmFinalizarAtendimento.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				FinalizaAtendimentoUI finAtendimentoUI = new FinalizaAtendimentoUI();
				finAtendimentoUI.setVisible(true);
				contentPane.add(finAtendimentoUI,0);
			}
		});
		mnReservaVaga.add(mntmFinalizarAtendimento);
		
		JMenuItem mntmConsulta = new JMenuItem("Atendimentos Finalizados");
		mntmConsulta.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				ConsultaAtendimentoUI conAtendimentoUI = new ConsultaAtendimentoUI();
				conAtendimentoUI.setVisible(true);
				contentPane.add(conAtendimentoUI,0);
			}
		});
		mnReservaVaga.add(mntmConsulta);
		
		JMenuItem mntmReservarVaga = new JMenuItem("Reservar Vaga");
		mnReservaVaga.add(mntmReservarVaga);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
	}

}
