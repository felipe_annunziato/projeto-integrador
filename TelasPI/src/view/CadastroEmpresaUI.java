package view;

import javax.swing.JInternalFrame;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JPanel;
import javax.swing.border.TitledBorder;

import controller.EmpresaController;
import controller.VagaController;

import javax.swing.border.LineBorder;
import java.awt.Color;
import java.awt.EventQueue;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import model.Empresa;
import model.Vaga;

public class CadastroEmpresaUI extends JInternalFrame{
	private JTextField text_rz;
	private JTextField text_endereco;
	private JTextField textTelefone;
	private JTextField text_email;
	private Empresa empresa;
	private JTextField text_cnpj;

	//Inicie o aplicativo.
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					CadastroEmpresaUI frame = new CadastroEmpresaUI(null);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	//Criando o Frame
	public CadastroEmpresaUI(Empresa empresaParaEditar) {
		setClosable(true);
		this.empresa = empresaParaEditar;
		setTitle("Cadastro Empresa");
		setBounds(100, 120, 450, 264);

		JPanel panelEmpresa = new JPanel();
		panelEmpresa.setBorder(new TitledBorder(new LineBorder(new Color(184, 207, 229)), "", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(51, 51, 51)));
		panelEmpresa.setToolTipText("");
		
		JButton btnCadastrar = new JButton("Cadastrar");
		btnCadastrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(empresa != null) {
					empresa.setRazaoSocial(text_rz.getText());
					empresa.setEndereco(text_endereco.getText());
					empresa.setTelefone(textTelefone.getText());
					empresa.setCnpj(text_cnpj.getText());
					empresa.setEmail(text_email.getText());
					new EmpresaController().editar(empresa);
					
					JOptionPane.showMessageDialog(null, "Empresa editada com sucesso!");
					dispose();
				} else {
					Empresa empresa = new Empresa();
					empresa.setRazaoSocial(text_rz.getText());
					empresa.setEndereco(text_endereco.getText());
					empresa.setTelefone(textTelefone.getText());
					empresa.setCnpj(text_cnpj.getText());
					empresa.setEmail(text_email.getText());
					new EmpresaController().cadastrar(empresa);
					JOptionPane.showMessageDialog(null, "Empresa Cadastrada com Sucesso!");
					dispose();
				}
			}
		}
	);

		JButton btnCancelar = new JButton("Cancelar");
		btnCancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				dispose();
			}
		});

		GroupLayout groupLayout = new GroupLayout(getContentPane());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(panelEmpresa, GroupLayout.PREFERRED_SIZE, 418, GroupLayout.PREFERRED_SIZE)
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(btnCadastrar)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(btnCancelar)))
					.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.TRAILING)
				.addGroup(Alignment.LEADING, groupLayout.createSequentialGroup()
					.addContainerGap()
					.addComponent(panelEmpresa, GroupLayout.PREFERRED_SIZE, 167, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(btnCadastrar)
						.addComponent(btnCancelar))
					.addContainerGap(16, Short.MAX_VALUE))
		);

		JLabel lblRazoSocial = new JLabel("Razão Social");

		text_rz = new JTextField();
		text_rz.setColumns(10);

		JLabel lblEndereo = new JLabel("Endereço");

		text_endereco = new JTextField();
		text_endereco.setColumns(10);

		JLabel lblTelefone = new JLabel("Telefone");

		textTelefone = new JTextField();
		textTelefone.setColumns(10);

		JLabel lblEmail = new JLabel("Email");

		text_email = new JTextField();
		text_email.setColumns(10);

		JLabel lblCnpj = new JLabel("Cnpj");

		text_cnpj = new JTextField();
		text_cnpj.setColumns(10);
		GroupLayout gl_panel = new GroupLayout(panelEmpresa);
		gl_panel.setHorizontalGroup(
			gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
						.addComponent(textTelefone, GroupLayout.DEFAULT_SIZE, 187, Short.MAX_VALUE)
						.addComponent(lblRazoSocial)
						.addComponent(text_rz, GroupLayout.DEFAULT_SIZE, 187, Short.MAX_VALUE)
						.addComponent(lblTelefone))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_panel.createSequentialGroup()
							.addComponent(lblCnpj)
							.addContainerGap())
						.addGroup(gl_panel.createSequentialGroup()
							.addComponent(lblEndereo)
							.addContainerGap())
						.addGroup(gl_panel.createSequentialGroup()
							.addComponent(lblEmail)
							.addGap(168))
						.addGroup(gl_panel.createSequentialGroup()
							.addGroup(gl_panel.createParallelGroup(Alignment.TRAILING)
								.addComponent(text_endereco, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 193, Short.MAX_VALUE)
								.addComponent(text_email, 193, 193, 193))
							.addContainerGap())
						.addGroup(gl_panel.createSequentialGroup()
							.addComponent(text_cnpj, GroupLayout.DEFAULT_SIZE, 193, Short.MAX_VALUE)
							.addContainerGap())))
		);
		gl_panel.setVerticalGroup(
			gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_panel.createParallelGroup(Alignment.TRAILING)
						.addComponent(lblEndereo)
						.addComponent(lblRazoSocial))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
						.addComponent(text_endereco, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(text_rz, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
						.addComponent(lblEmail)
						.addComponent(lblTelefone))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
						.addComponent(text_email, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(textTelefone, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblCnpj)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(text_cnpj, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(69, Short.MAX_VALUE))
		);
		panelEmpresa.setLayout(gl_panel);
		getContentPane().setLayout(groupLayout);

		if(empresaParaEditar != null) {
			text_cnpj.setText(empresaParaEditar.getCnpj());
			text_email.setText(empresaParaEditar.getEmail());
			text_endereco.setText(empresaParaEditar.getEndereco());
			text_rz.setText(empresaParaEditar.getRazaoSocial());
			textTelefone.setText(empresaParaEditar.getTelefone());
		}
		
	}
	
	
}
