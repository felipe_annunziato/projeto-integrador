package view;

import java.awt.EventQueue;

import javax.swing.JInternalFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import javax.swing.JScrollPane;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import controller.EmpresaController;
import model.Empresa;
import view.tables.EmpresaTableModel;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.LayoutStyle.ComponentPlacement;

public class ConsultaEmpresaUI extends JInternalFrame {
	private JTable table_Consulta;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ConsultaEmpresaUI frame = new ConsultaEmpresaUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public ConsultaEmpresaUI() {
		setTitle("Consulta Empresa");
		setClosable(true);
		setBounds(100, 100, 450, 263);
		
		JScrollPane scrollpaneConsulta = new JScrollPane();
		
		JButton btnEditar = new JButton("Editar");
		btnEditar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Empresa empresa = new EmpresaTableModel(new EmpresaController().buscarTodos()).get(table_Consulta.getSelectedRow());
				CadastroEmpresaUI cadEmpresa = new CadastroEmpresaUI(empresa);
				cadEmpresa.setVisible(true);
				getParent().add(cadEmpresa, 0);
				dispose();
			}
		});
		
		JButton btnExcluir = new JButton("Excluir");
		btnExcluir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Empresa empresa = new EmpresaTableModel(new EmpresaController().buscarTodos()).get(table_Consulta.getSelectedRow());
				new EmpresaController().excluir(empresa.getIdEmpresa());
				
				table_Consulta.setModel(new EmpresaTableModel(new EmpresaController().buscarTodos()));
				JOptionPane.showMessageDialog(null, "Empresa excluída com Sucesso!");
			}
		});
		
		
		JButton btnFechar = new JButton("Fechar");
		btnFechar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		GroupLayout groupLayout = new GroupLayout(getContentPane());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(54)
					.addComponent(btnEditar)
					.addGap(51)
					.addComponent(btnExcluir)
					.addPreferredGap(ComponentPlacement.RELATED, 63, Short.MAX_VALUE)
					.addComponent(btnFechar)
					.addGap(35))
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addComponent(scrollpaneConsulta, GroupLayout.DEFAULT_SIZE, 416, Short.MAX_VALUE)
					.addContainerGap())
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(6)
					.addComponent(scrollpaneConsulta, GroupLayout.PREFERRED_SIZE, 164, GroupLayout.PREFERRED_SIZE)
					.addGap(8)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(btnEditar)
						.addComponent(btnExcluir)
						.addComponent(btnFechar))
					.addContainerGap(18, Short.MAX_VALUE))
		);
		
		table_Consulta = new JTable();
		table_Consulta.setModel(new EmpresaTableModel(new EmpresaController().buscarTodos()));
		scrollpaneConsulta.setViewportView(table_Consulta);
		getContentPane().setLayout(groupLayout);

	}
}
