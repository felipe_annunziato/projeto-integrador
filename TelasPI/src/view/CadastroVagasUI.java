package view;

import java.awt.EventQueue;

import javax.swing.JInternalFrame;
import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JPanel;
import javax.swing.JComboBox;
import java.awt.Color;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.LayoutStyle.ComponentPlacement;

import controller.AtendimentoController;
import controller.VagaController;
import model.Empresa;
import model.Vaga;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class CadastroVagasUI extends JInternalFrame {
	private JTextField textFieldNVagas;
	private JTextField txtValorHora;
	private AtendimentoController atendimentoController = new AtendimentoController();
	private VagaController vagaController = new VagaController();
	private Vaga vaga;
	private JComboBox<Empresa> comboBoxEmpresa;
	private JTextField txtValorDiaria;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					CadastroVagasUI frame = new CadastroVagasUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public CadastroVagasUI() {
		setTitle("Cadastro Vagas");
		setClosable(true);
		setBounds(100, 100, 450, 300);
		
		JPanel panel = new JPanel();
		
		JButton btnCadastrar = new JButton("Cadastrar");
		btnCadastrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {	
			Empresa empresaSelecionada = (Empresa) comboBoxEmpresa.getSelectedItem();
			int nVaga = Integer.parseInt(textFieldNVagas.getText());
			try {
				Vaga vaga = new Vaga();
				vaga.setIdEmpresa(empresaSelecionada.getIdEmpresa());
				vaga.setIdGrupo(empresaSelecionada.getIdGrupo());
				vaga.setStatus("free");
				vaga.setValor_hora(Double.parseDouble(txtValorHora.getText()));
				vaga.setValor_diaria(Double.parseDouble(txtValorDiaria.getText()));
				for(int i=0;i<nVaga;i++) {
					vagaController.cadastraVagas(vaga);
				}
				JOptionPane.showMessageDialog(null, "Vaga Cadastrada");
				dispose();
			} catch (Exception e) {
				System.out.println(e.getMessage());
			}
				
			}
		});
		
		JButton btnCancelar = new JButton("Cancelar");
		btnCancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		GroupLayout groupLayout = new GroupLayout(getContentPane());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(panel, GroupLayout.PREFERRED_SIZE, 415, GroupLayout.PREFERRED_SIZE)
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(btnCadastrar)
							.addPreferredGap(ComponentPlacement.UNRELATED)
							.addComponent(btnCancelar)))
					.addContainerGap(13, Short.MAX_VALUE))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addComponent(panel, GroupLayout.PREFERRED_SIZE, 219, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(btnCadastrar)
						.addComponent(btnCancelar))
					.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
		);
		
		DefaultComboBoxModel<Empresa> modelEmpresa = new DefaultComboBoxModel<Empresa>();
		for(Empresa emp : atendimentoController.listaEmpresa()) {
			modelEmpresa.addElement(emp);
		}
		
	    comboBoxEmpresa = new JComboBox<Empresa>();
		comboBoxEmpresa.setBackground(Color.WHITE);
		comboBoxEmpresa.setModel(modelEmpresa);
		
		textFieldNVagas = new JTextField();
		textFieldNVagas.setColumns(10);
		
		txtValorHora = new JTextField();
		txtValorHora.setColumns(10);
		
		JLabel lblEmpresa = new JLabel("Empresa");
		
		JLabel lblQuantidadeDeVagas = new JLabel("Quantidade de Vagas");
		
		JLabel lblValor = new JLabel("Valor Hora");
		
		txtValorDiaria = new JTextField();
		txtValorDiaria.setColumns(10);
		
		JLabel lblValorDiria = new JLabel("Valor Diária");
		GroupLayout gl_panel = new GroupLayout(panel);
		gl_panel.setHorizontalGroup(
			gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
						.addComponent(comboBoxEmpresa, GroupLayout.PREFERRED_SIZE, 318, GroupLayout.PREFERRED_SIZE)
						.addComponent(textFieldNVagas, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(lblEmpresa)
						.addComponent(lblQuantidadeDeVagas)
						.addGroup(gl_panel.createSequentialGroup()
							.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
								.addComponent(txtValorHora, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(lblValor))
							.addGap(44)
							.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
								.addComponent(lblValorDiria)
								.addComponent(txtValorDiaria, GroupLayout.PREFERRED_SIZE, 132, GroupLayout.PREFERRED_SIZE))))
					.addContainerGap(85, Short.MAX_VALUE))
		);
		gl_panel.setVerticalGroup(
			gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel.createSequentialGroup()
					.addContainerGap()
					.addComponent(lblEmpresa)
					.addGap(5)
					.addComponent(comboBoxEmpresa, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addGap(11)
					.addComponent(lblQuantidadeDeVagas)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(textFieldNVagas, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addGap(18)
					.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblValor)
						.addComponent(lblValorDiria))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
						.addComponent(txtValorHora, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(txtValorDiaria, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addContainerGap(54, Short.MAX_VALUE))
		);
		panel.setLayout(gl_panel);
		getContentPane().setLayout(groupLayout);

	}

}
