package view.tables;

import javax.swing.table.AbstractTableModel;
import java.util.ArrayList;
import java.util.List;

import model.Atendimento;
import model.Empresa;
public class EmpresaTableModel extends AbstractTableModel{

	private static final int COL_RAZAOSOCIAL = 0;
	private static final int COL_CNPJ = 1;
	private static final int COL_ENDERECO = 2;
	private static final int COL_TELEFONE = 3;
	private static final int COL_EMAIL = 4;

	private List<Empresa>value;

	public EmpresaTableModel(List<Empresa>value) {
		this.value = new ArrayList<Empresa>(value);
	}

	public int getRowCount() {
		return value.size();
	}

	public int getColumnCount() {
		return 5;
	}

	@Override
	public Object getValueAt(int row, int column) {
		Empresa empresa = value.get(row);

		if(column == COL_RAZAOSOCIAL)
			return empresa.getRazaoSocial();
		else
			if(column == COL_CNPJ)
				return empresa.getCnpj();
			else
				if(column == COL_ENDERECO) 
					return empresa.getEndereco();
				else
					if(column == COL_TELEFONE)
						return empresa.getTelefone();
					else
						if(column == COL_EMAIL)
							return empresa.getEmail();
		return "";						
	}

	public String getColumnName(int column) {
		if (column == COL_RAZAOSOCIAL) return "Razão Social";
		if (column == COL_CNPJ) return "Cnpj";
		if (column == COL_ENDERECO) return "Endereço";
		if (column == COL_TELEFONE) return "Telefone";
		if (column == COL_EMAIL) return "Email";
		return ""; //Nunca deve ocorrer
	}

	public void setValueAT(Object aValue, int rowIndex, int columnIndex) {
		Empresa empresa = value.get(rowIndex);
		try {
			if(columnIndex == COL_RAZAOSOCIAL)
				empresa.setRazaoSocial(aValue.toString());
			else
				if(columnIndex == COL_CNPJ)
					empresa.setCnpj(aValue.toString());
				else
					if(columnIndex == COL_ENDERECO)
						empresa.setEndereco(aValue.toString());
					else
						if(columnIndex == COL_TELEFONE)
							empresa.setTelefone(aValue.toString());
						else

							if(columnIndex == COL_EMAIL)
								empresa.setEmail(aValue.toString());
		}catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	public Class<?> getColumnClass(int columnIndex) {
		//Qual a classe das nossas colunas? Como estamos exibindo texto, � string.
		return String.class;
	}
	public boolean isCellEditable(int rowIndex, int columnIndex) {
		//Indicamos se a c�lula da rowIndex e da columnIndex � edit�vel. Nossa tabela toda �.
		return true;
	}
	public Empresa get(int row) {
		return value.get(row);
	}
}


