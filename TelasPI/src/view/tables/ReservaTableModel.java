package view.tables;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.swing.table.AbstractTableModel;

import model.Empresa;
import model.ReservaVaga;
import view.ReservaUI;

public class ReservaTableModel extends AbstractTableModel{

	private static final int COL_EMPRESA = 0;
	private static final int COL_IDVAGA = 1;
	private static final int COL_CLIENTE = 2;
	private static final int COL_PLACA = 3;
	private static final int COL_DATA = 4;
	private static final int COL_HORA = 5;

	private List<ReservaVaga> value;

	SimpleDateFormat sdh = new SimpleDateFormat("hh:mm");
	SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

	public ReservaTableModel(List<ReservaVaga>value) {
		this.value = new ArrayList<ReservaVaga>(value);
	}

	public int getRowCount() {
		return value.size();
	}

	public int getColumnCount() {
		return 6;
	}

	public Object getValueAt(int row, int column) {
		ReservaVaga rvaga = value.get(row);

		if(column == COL_EMPRESA)
			return rvaga.getEmpresa().getRazaoSocial();
		else
			if(column == COL_IDVAGA)
				return rvaga.getVaga().getIdVaga();
			else
				if(column == COL_CLIENTE)
					return rvaga.getUserCliente().getNome();
				else
					if(column == COL_PLACA)
						return rvaga.getPlaca();
					else
						if(column == COL_DATA)
							return rvaga.getData();
						else
							if(column == COL_HORA)
								return rvaga.getHora();
		return "";
	}

	public String getColumnName(int column) {
		if (column == COL_EMPRESA) return "Empresa";
		if (column == COL_IDVAGA) return "Vaga";
		if (column == COL_CLIENTE) return "Cliente";
		if (column == COL_PLACA) return "Placa";
		if (column == COL_DATA) return "Data da reserva";
		if (column == COL_HORA) return "Hora da reserva";
		return ""; 
	}

	public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
		ReservaVaga rvaga = value.get(rowIndex);
		try {
			if(columnIndex == COL_EMPRESA)
				rvaga.getEmpresa().setRazaoSocial(aValue.toString());
			else
				if(columnIndex == COL_IDVAGA)
					rvaga.getVaga().setIdVaga(Integer.parseInt(aValue.toString()));
				else
					if(columnIndex == COL_CLIENTE)
						rvaga.getUserCliente().setNome(aValue.toString());
					else
						if(columnIndex == COL_PLACA)
							rvaga.setPlaca(aValue.toString());
						else
							if(columnIndex == COL_DATA)
								rvaga.setData(sdf.parse(aValue.toString()));
							else
								if(columnIndex == COL_HORA)
									rvaga.setHora(sdh.parse(aValue.toString()));
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	public Class<?> getColumnClass(int columnIndex) {
		return String.class;
	}
	public boolean isCellEditable(int rowIndex, int columnIndex) {
		return true;
	}

	public ReservaVaga get(int row) {
		return value.get(row);
	}

}
