package view.tables;

import java.util.ArrayList;
import java.util.List;

import javax.swing.table.AbstractTableModel;

import model.Vaga;

public class VagaTableModel extends AbstractTableModel {

	private static final long serialVersionUID = 1L;
	private static int COL_ID = 1;
	private static int COL_NOME = 0;
	private static int COL_STATUS = 2;
	private static int COL_VALORDIARIA = 3;
	private static int COL_VALORHORA = 4;


	private List<Vaga> valores;	

	public VagaTableModel(List<Vaga> valores) {
		this.valores = new ArrayList<Vaga>(valores);
	}

	@Override
	public int getColumnCount() {
		// TODO Auto-generated method stub
		return 5;
	}

	@Override
	public int getRowCount() {
		return valores.size();
	}

	public String getColumnName(int column) {
		//Qual � o nome das nossas colunas?
		if (column == COL_NOME) return "Empresa";
		if (column == COL_ID) return "ID Vaga";
		if (column == COL_STATUS) return "Status";
		if (column == COL_VALORHORA) return "Valor hora";
		if (column == COL_VALORDIARIA) return "Valor diaria";
		return ""; //Nunca deve ocorrer
	}


	public Object getValueAt(int row, int column) {
		//Precisamos retornar o valor da coluna column e da linha row.
		Vaga vaga = valores.get(row);
		if (column == COL_NOME)
			return vaga.getRazaoSocial();
		else 
			if (column == COL_STATUS) 
				return vaga.getStatus();
			else
				if (column == COL_ID)
					return vaga.getIdVaga();
				else
					if (column == COL_VALORHORA)
						return vaga.getValor_hora();
					else
						if (column == COL_VALORDIARIA)
							return vaga.getValor_diaria();

		return ""; //Nunca deve ocorrer
	}


	public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
		Vaga vaga = valores.get(rowIndex);
		try {
			if (columnIndex == COL_NOME)
				vaga.setRazaoSocial(aValue.toString());
			else 
				if (columnIndex == COL_ID) 
					vaga.setIdVaga(Integer.parseInt(aValue.toString()));
				else
					if (columnIndex == COL_STATUS)
						vaga.setStatus(aValue.toString());
					else
						if (columnIndex == COL_VALORHORA)
							vaga.setValor_hora(Double.parseDouble(aValue.toString()));
						else
							if (columnIndex == COL_VALORDIARIA)
								vaga.setValor_diaria(Double.parseDouble(aValue.toString()));
		}catch (Exception e) {
			System.out.println(e.getMessage());
		}

	}

	public Class<?> getColumnClass(int columnIndex) {
		//Qual a classe das nossas colunas? Como estamos exibindo texto, � string.
		return String.class;
	}

	public boolean isCellEditable(int rowIndex, int columnIndex) {
		//Indicamos se a célula da rowIndex e da columnIndex é editável..
		return true;
	}

	public Vaga get(int row) {
		return valores.get(row);
	}
}
