package view.tables;

import java.util.ArrayList;
import java.util.List;

import javax.swing.table.AbstractTableModel;

import model.UserCliente;

public class ClienteTableModel extends AbstractTableModel{
	
	private static final long serialVersionUID = 1L;
	private static final int COL_NOME = 0;
	private static final int COL_USER = 1;
	private static final int COL_CPF = 2;
	private static final int COL_EMAIL = 3;
	private static final int COL_TELEFONE = 4;

	
	

	private List<UserCliente>valores;       

	//Esse é um construtor, que recebe a nossa lista de clientes
	public ClienteTableModel(List<UserCliente> valores) {
		this.valores = new ArrayList<UserCliente>(valores);
	}

	public int getRowCount() {
		//Quantas linhas tem sua tabela? Uma para cada item da lista.
		return valores.size();
	}

	public int getColumnCount() {
		//Quantas colunas tem a tabela?
		return 5;
	}

	public String getColumnName(int column) {
		//Qual � o nome das nossas colunas?
		if (column == COL_NOME) return "Nome do Cliente";
		if (column == COL_CPF) return "CPF";
		if (column == COL_USER) return "Username";
		if (column == COL_EMAIL) return "Email";
		if (column == COL_TELEFONE) return "Telefone";
		return ""; //Nunca deve ocorrer
	}

	public Object getValueAt(int row, int column) {
		//Precisamos retornar o valor da coluna column e da linha row.
		UserCliente cliente = valores.get(row);
		if (column == COL_NOME)
			return cliente.getNome();
		else 
			if (column == COL_CPF) 
					return cliente.getCpf();
			else
				if (column == COL_USER)
						return cliente.getUsername();
				else 
					if (column == COL_EMAIL)
						return cliente.getEmail();
					else
						if (column == COL_TELEFONE)
							return cliente.getTelefone();
			
		return ""; //Nunca deve ocorrer
	}

	public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
		UserCliente cliente = valores.get(rowIndex);
		//Vamos alterar o valor da coluna columnIndex na linha rowIndex com o valor aValue passado no par�metro.
		//Note que vc poderia alterar 2 campos ao inv�s de um s�.
		if (columnIndex == COL_NOME)
			cliente.setNome(aValue.toString());
		else 
			if (columnIndex == COL_CPF) 
				cliente.setCpf(aValue.toString());
			else
				if (columnIndex == COL_USER)
					cliente.setUsername(aValue.toString());
				else
					if (columnIndex == COL_EMAIL)
						cliente.setEmail(aValue.toString());
					else
						if (columnIndex == COL_TELEFONE)
							cliente.setTelefone(aValue.toString());
		
	}

	public Class<?> getColumnClass(int columnIndex) {
		//Qual a classe das nossas colunas? Como estamos exibindo texto, � string.
		return String.class;
	}

	public boolean isCellEditable(int rowIndex, int columnIndex) {
		//Indicamos se a célula da rowIndex e da columnIndex é editável..
		return true;
	}
	//Já que esse tableModel é de clientes, vamos fazer um get que retorne um objeto cliente inteiro.
	//Isso elimina a necessidade de chamar o getValueAt() nas telas. 
	public UserCliente get(int row) {
		return valores.get(row);
	}
}


