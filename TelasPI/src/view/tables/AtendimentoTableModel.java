package view.tables;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import javax.swing.table.AbstractTableModel;
import model.Atendimento;
import model.UserCliente;

public class AtendimentoTableModel extends AbstractTableModel {

	private static int COL_NOME = 0;
	private static int COL_PLACA = 1;
	private static int COL_IDVAGA = 2;
	private static int COL_HORA_ENT = 3;
	private static int COL_DATA_ENT = 4;
	private static int COL_HORA_SAI = 5;
	private static int COL_DATA_SAI = 6;
	private static int COL_VALOR = 7;

	SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
	SimpleDateFormat sdh = new SimpleDateFormat("HH:mm");


	private List<Atendimento> var;

	public AtendimentoTableModel(List<Atendimento> var) {
		this.var = new ArrayList<Atendimento>(var);
	}

	@Override
	public int getColumnCount() {
		return 8;
	}

	@Override
	public int getRowCount() {
		return var.size();
	}

	@Override
	public Object getValueAt(int row, int column) {
		Atendimento atendimento = var.get(row);

		if (column == COL_NOME)
			return atendimento.getCliente().getNome();
		else 
			if (column == COL_PLACA) 
				return atendimento.getPlaca();
			else
				if (column == COL_IDVAGA) 
					return atendimento.getVaga().getIdVaga();
				else
					if(column == COL_HORA_ENT)
						return atendimento.getHoraEntrada();
					else
						if(column == COL_DATA_ENT)
							return atendimento.getDataEntrada();
						else
							if(column == COL_HORA_SAI)
								return atendimento.getHoraSaida();
							else
								if(column == COL_DATA_SAI)
									return atendimento.getDataSaida();
								else
									if(column == COL_VALOR)
										return atendimento.getValor();

		return "";
	}

	public String getColumnName(int column) {
		if (column == COL_NOME) return "Nome do Cliente";
		if (column == COL_PLACA) return "Placa";
		if (column == COL_IDVAGA) return "Vaga";
		if (column == COL_HORA_ENT) return "Hora Entrada";
		if (column == COL_DATA_ENT) return "Data Entrada";
		if (column == COL_HORA_SAI) return "Hora Saída";
		if (column == COL_DATA_SAI) return "Data Saída";
		if (column == COL_VALOR) return "Valor";
		return "";
	}

	public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
		Atendimento atendimento = var.get(rowIndex);
		try {
			if(columnIndex == COL_NOME)
				atendimento.getCliente().setNome(aValue.toString());
			else
				if(columnIndex == COL_PLACA)
					atendimento.setPlaca(aValue.toString());
				else
					if(columnIndex == COL_IDVAGA)
						atendimento.getVaga().setIdVaga(Integer.parseInt(aValue.toString()));
					else
						if(columnIndex == COL_HORA_ENT)
							atendimento.setHoraEntrada(aValue.toString());
						else
							if(columnIndex == COL_DATA_ENT)
								atendimento.setDataEntrada(sdf.parse(aValue.toString()));
							else
								if(columnIndex == COL_HORA_SAI)
									atendimento.setHoraSaida(aValue.toString());
								else
									if(columnIndex == COL_DATA_SAI)
										atendimento.setDataSaida(sdf.parse(aValue.toString()));
									else
										if(columnIndex == COL_VALOR)
											atendimento.setValor(Double.parseDouble(aValue.toString()));
		}catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}
	public Class<?> getColumnClass(int columnIndex) {
		return String.class;
	}
	public boolean isCellEditable(int rowIndex, int columnIndex) {
		return true;
	}
	public Atendimento get(int row) {
		return var.get(row);
	}

}
