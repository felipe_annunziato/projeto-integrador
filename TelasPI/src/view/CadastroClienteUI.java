package view;

import java.awt.EventQueue;

import javax.swing.JInternalFrame;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import java.awt.Panel;
import javax.swing.BoxLayout;
import javax.swing.border.BevelBorder;
import javax.swing.JTextPane;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;

import controller.ClienteController;
import model.UserCliente;

import javax.swing.border.LineBorder;
import java.awt.Color;
import java.awt.Label;
import javax.swing.JTextArea;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class CadastroClienteUI extends JInternalFrame {
	private JTextField txtNome;
	private JTextField txtCpf;
	private JTextField txtUsername;
	private JTextField txtEmail;
	private JTextField txtTelefone;
	private UserCliente cliente;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					CadastroClienteUI frame = new CadastroClienteUI(null);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 * @param cliente2 
	 */
	public CadastroClienteUI(UserCliente clienteParaEditar) {
		this.cliente = clienteParaEditar;
		setTitle("AutoParking");
		setBounds(100, 100, 664, 454);
		
		JPanel panelCliente = new JPanel();
		panelCliente.setBorder(new TitledBorder(new LineBorder(new Color(184, 207, 229)), "Cadastro Cliente", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(51, 51, 51)));
		panelCliente.setToolTipText("");
		
		JButton btnSalvar = new JButton("Salvar");
		btnSalvar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (cliente != null){
					cliente.setNome(txtNome.getText());
					cliente.setCpf(txtCpf.getText());
					cliente.setEmail(txtEmail.getText());
					cliente.setTelefone(txtTelefone.getText());
					cliente.setUsername(txtUsername.getText());
					new ClienteController().editar(cliente);
					JOptionPane.showMessageDialog(null, "Cliente editado com sucesso");
					ConsultaClienteUI conCliente = new ConsultaClienteUI();
					conCliente.setVisible(true);
					getParent().add(conCliente, 0);		
				}else {
				cliente  =  new UserCliente();
				cliente.setNome(txtNome.getText());
				cliente.setCpf(txtCpf.getText());
				cliente.setEmail(txtEmail.getText());
				cliente.setTelefone(txtTelefone.getText());
				cliente.setUsername(txtUsername.getText());
				new ClienteController().salvar(cliente);				
				JOptionPane.showMessageDialog(null, "Cliente cadastrado com sucesso");;
				dispose();
				} 
			
		}});
		
		JButton btnCancelar = new JButton("Cancelar");
		btnCancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
					
				
			}
		});
		GroupLayout groupLayout = new GroupLayout(getContentPane());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(panelCliente, GroupLayout.PREFERRED_SIZE, 561, GroupLayout.PREFERRED_SIZE)
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(btnSalvar)
							.addPreferredGap(ComponentPlacement.UNRELATED)
							.addComponent(btnCancelar)))
					.addContainerGap(81, Short.MAX_VALUE))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(22)
					.addComponent(panelCliente, GroupLayout.PREFERRED_SIZE, 244, GroupLayout.PREFERRED_SIZE)
					.addGap(18)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(btnSalvar)
						.addComponent(btnCancelar))
					.addContainerGap(113, Short.MAX_VALUE))
		);
		
		JLabel lblNome = new JLabel("Nome:");
		
		JLabel lblUsername = new JLabel("Username:");
		
		JLabel lblCpf = new JLabel("CPF:");
		
		JLabel lblTelefone = new JLabel("Telefone:");
		
		JLabel lblEmail = new JLabel("Email:");
		
		txtNome = new JTextField();
		txtNome.setColumns(10);
		
		txtCpf = new JTextField();
		txtCpf.setColumns(10);
		
		txtUsername = new JTextField();
		txtUsername.setColumns(10);
		
		txtEmail = new JTextField();
		txtEmail.setColumns(10);
		
		txtTelefone = new JTextField();
		txtTelefone.setColumns(10);
		GroupLayout gl_panelCliente = new GroupLayout(panelCliente);
		gl_panelCliente.setHorizontalGroup(
			gl_panelCliente.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panelCliente.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_panelCliente.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_panelCliente.createSequentialGroup()
							.addGroup(gl_panelCliente.createParallelGroup(Alignment.LEADING)
								.addGroup(gl_panelCliente.createSequentialGroup()
									.addGroup(gl_panelCliente.createParallelGroup(Alignment.LEADING)
										.addComponent(lblUsername)
										.addComponent(lblNome))
									.addGap(106))
								.addGroup(Alignment.TRAILING, gl_panelCliente.createSequentialGroup()
									.addGroup(gl_panelCliente.createParallelGroup(Alignment.TRAILING)
										.addComponent(txtUsername, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 136, Short.MAX_VALUE)
										.addComponent(txtNome, GroupLayout.DEFAULT_SIZE, 136, Short.MAX_VALUE))
									.addGap(47)))
							.addGroup(gl_panelCliente.createParallelGroup(Alignment.LEADING)
								.addGroup(gl_panelCliente.createSequentialGroup()
									.addComponent(lblTelefone)
									.addContainerGap(289, Short.MAX_VALUE))
								.addGroup(gl_panelCliente.createSequentialGroup()
									.addComponent(lblCpf)
									.addContainerGap())
								.addGroup(Alignment.TRAILING, gl_panelCliente.createSequentialGroup()
									.addGroup(gl_panelCliente.createParallelGroup(Alignment.TRAILING)
										.addComponent(txtCpf, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 271, Short.MAX_VALUE)
										.addComponent(txtTelefone, GroupLayout.DEFAULT_SIZE, 271, Short.MAX_VALUE))
									.addGap(85))))
						.addGroup(gl_panelCliente.createSequentialGroup()
							.addComponent(lblEmail)
							.addContainerGap(497, Short.MAX_VALUE))
						.addGroup(gl_panelCliente.createSequentialGroup()
							.addComponent(txtEmail, GroupLayout.DEFAULT_SIZE, 527, Short.MAX_VALUE)
							.addContainerGap())))
		);
		gl_panelCliente.setVerticalGroup(
			gl_panelCliente.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panelCliente.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_panelCliente.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblNome)
						.addComponent(lblTelefone))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_panelCliente.createParallelGroup(Alignment.BASELINE)
						.addComponent(txtNome, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(txtTelefone, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addGroup(gl_panelCliente.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblUsername)
						.addComponent(lblCpf))
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addGroup(gl_panelCliente.createParallelGroup(Alignment.BASELINE)
						.addComponent(txtUsername, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(txtCpf, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(lblEmail)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(txtEmail, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addGap(93))
		);
		panelCliente.setLayout(gl_panelCliente);
		getContentPane().setLayout(groupLayout);
		
		if (clienteParaEditar != null) {
			txtCpf.setText(clienteParaEditar.getCpf());
			txtEmail.setText(clienteParaEditar.getEmail());
			txtNome.setText(clienteParaEditar.getNome());
			txtTelefone.setText(clienteParaEditar.getTelefone());
			txtUsername.setText(clienteParaEditar.getUsername());
		}

	}

	public UserCliente getCliente() {
		return cliente;
	}

	public void setCliente(UserCliente cliente) {
		this.cliente = cliente;
	}
}
