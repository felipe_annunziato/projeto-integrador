package view;

import javax.swing.JInternalFrame;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JPanel;
import javax.swing.border.TitledBorder;

import controller.AtendimentoController;
import view.tables.AtendimentoTableModel;

import javax.swing.border.LineBorder;
import java.awt.Color;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JButton;
import javax.swing.LayoutStyle.ComponentPlacement;

public class ConsultaAtendimentoUI extends JInternalFrame{
	private JTable tableAtendimento;
	public ConsultaAtendimentoUI() {
		setClosable(true);
		setTitle("Atendimentos FInalizados");
		
		JPanel panelAtendimento = new JPanel();
		GroupLayout groupLayout = new GroupLayout(getContentPane());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addComponent(panelAtendimento, GroupLayout.DEFAULT_SIZE, 667, Short.MAX_VALUE)
					.addContainerGap())
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addComponent(panelAtendimento, GroupLayout.DEFAULT_SIZE, 269, Short.MAX_VALUE)
					.addContainerGap())
		);
		
		JScrollPane scrollPaneAtendimento = new JScrollPane();
		GroupLayout gl_panelAtendimento = new GroupLayout(panelAtendimento);
		gl_panelAtendimento.setHorizontalGroup(
			gl_panelAtendimento.createParallelGroup(Alignment.LEADING)
				.addComponent(scrollPaneAtendimento, GroupLayout.DEFAULT_SIZE, 667, Short.MAX_VALUE)
		);
		gl_panelAtendimento.setVerticalGroup(
			gl_panelAtendimento.createParallelGroup(Alignment.LEADING)
				.addComponent(scrollPaneAtendimento, GroupLayout.DEFAULT_SIZE, 269, Short.MAX_VALUE)
		);
		
		tableAtendimento = new JTable();
		tableAtendimento.setModel(new AtendimentoTableModel(new AtendimentoController().listaAtendimentosFechados()));
		scrollPaneAtendimento.setViewportView(tableAtendimento);
		panelAtendimento.setLayout(gl_panelAtendimento);
		getContentPane().setLayout(groupLayout);
	}
}
