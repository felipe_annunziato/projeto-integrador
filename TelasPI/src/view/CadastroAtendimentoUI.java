package view;

import javax.swing.JInternalFrame;
import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JPanel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;

import controller.AtendimentoController;
import controller.ClienteController;
import controller.VagaController;
import model.Atendimento;
import model.Empresa;
import model.UserCliente;
import model.Vaga;

import javax.swing.JComboBox;
import java.awt.Color;
import javax.swing.border.LineBorder;
import java.awt.SystemColor;
import javax.swing.border.MatteBorder;
import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;

public class CadastroAtendimentoUI extends JInternalFrame{
	private JTextField textFieldPlaca;

	private AtendimentoController atendimentoController = new AtendimentoController();
	private ClienteController clienteController = new ClienteController();
	private VagaController vagaController = new VagaController();
	private JComboBox<Empresa> comboBoxEmpresa;
	private JComboBox<UserCliente> comboBoxCliente;
	private JComboBox<Vaga> comboBoxVagas;

	public CadastroAtendimentoUI() {
		setBorder(new LineBorder(new Color(0, 0, 0)));
		setBackground(SystemColor.window);
		setMaximizable(true);
		setTitle("Atendimento");
		setRootPaneCheckingEnabled(false);
		setClosable(true);
		setBounds(100, 100, 450, 269);
		JPanel panel = new JPanel();
		panel.setBackground(SystemColor.controlHighlight);
		panel.setBorder(new MatteBorder(1, 1, 1, 1, (Color) Color.LIGHT_GRAY));

		JButton btnIniciar = new JButton("Iniciar");
		btnIniciar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Empresa empresaSelecionada = (Empresa) comboBoxEmpresa.getSelectedItem();
				UserCliente clienteSelecionado = (UserCliente) comboBoxCliente.getSelectedItem();
				Vaga vagaSelecionada = (Vaga) comboBoxVagas.getSelectedItem();
				try {
					Atendimento atendimento = new Atendimento();
					atendimento.setVaga(vagaSelecionada);
					atendimento.setPlaca(textFieldPlaca.getText());
					atendimentoController.iniciaAtendimento(empresaSelecionada, clienteSelecionado, atendimento);
					atendimentoController.atualizaStatusVagaBusy(vagaSelecionada);
					JOptionPane.showMessageDialog(getParent(), "Atendimento iniciado.\n"+vagaSelecionada.getIdVaga());
					dispose();
				}catch (Exception e) {
					System.out.println(e.getMessage());
				}
			}
		});

		JButton btnCancelar = new JButton("Cancelar");
		btnCancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				dispose();
			}
		});
		GroupLayout groupLayout = new GroupLayout(getContentPane());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(panel, GroupLayout.PREFERRED_SIZE, 424, GroupLayout.PREFERRED_SIZE)
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(btnIniciar)
							.addGap(18)
							.addComponent(btnCancelar)))
					.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addComponent(panel, GroupLayout.PREFERRED_SIZE, 180, GroupLayout.PREFERRED_SIZE)
					.addGap(18)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(btnIniciar)
						.addComponent(btnCancelar))
					.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
		);

		JLabel lblPlaca = new JLabel("Placa");

		textFieldPlaca = new JTextField();
		textFieldPlaca.setColumns(10);

		DefaultComboBoxModel<Empresa> modelEmpresa = new DefaultComboBoxModel<Empresa>();
		for(Empresa emp : atendimentoController.listaEmpresa()) {
			modelEmpresa.addElement(emp);
		}

		comboBoxEmpresa = new JComboBox<Empresa>();
		comboBoxEmpresa.setBackground(Color.WHITE);
		comboBoxEmpresa.setModel(modelEmpresa);

		JLabel lblEmpresa = new JLabel("Empresa");

		DefaultComboBoxModel<UserCliente> modelCliente = new DefaultComboBoxModel<UserCliente>();
		for(UserCliente c : clienteController.listarClientes()) {
			modelCliente.addElement(c);
		}

		comboBoxCliente = new JComboBox<UserCliente>();
		comboBoxCliente.setBackground(Color.WHITE);
		comboBoxCliente.setModel(modelCliente);

		JLabel lblCliente = new JLabel("Cliente");

		JButton btnVerificarReserva = new JButton("Verificar Reserva");
		btnVerificarReserva.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

			}
		});

		JLabel lblVaga = new JLabel("Vaga");

		DefaultComboBoxModel<Vaga> modelVaga = new DefaultComboBoxModel<Vaga>();
		Empresa empresaSelecionada = (Empresa) comboBoxEmpresa.getSelectedItem();
		for(Vaga v : vagaController.listarVagasLivres(empresaSelecionada)) {
			modelVaga.addElement(v);
		}

		comboBoxVagas = new JComboBox<Vaga>();
		comboBoxVagas.setBackground(Color.WHITE);
		comboBoxVagas.setModel(modelVaga);

		GroupLayout gl_panel = new GroupLayout(panel);
		gl_panel.setHorizontalGroup(
				gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel.createSequentialGroup()
						.addContainerGap()
						.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
								.addComponent(lblPlaca)
								.addComponent(lblVaga)
								.addGroup(gl_panel.createSequentialGroup()
										.addGroup(gl_panel.createParallelGroup(Alignment.TRAILING, false)
												.addComponent(comboBoxVagas, Alignment.LEADING, 0, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
												.addComponent(textFieldPlaca, Alignment.LEADING, 184, 184, Short.MAX_VALUE)
												.addComponent(lblEmpresa, Alignment.LEADING)
												.addComponent(comboBoxEmpresa, Alignment.LEADING, 0, 184, Short.MAX_VALUE))
										.addGap(18)
										.addGroup(gl_panel.createParallelGroup(Alignment.LEADING, false)
												.addComponent(lblCliente)
												.addComponent(comboBoxCliente, 0, 169, Short.MAX_VALUE)
												.addComponent(btnVerificarReserva, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
						.addContainerGap(39, Short.MAX_VALUE))
				);
		gl_panel.setVerticalGroup(
				gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel.createSequentialGroup()
						.addGap(17)
						.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
								.addComponent(lblEmpresa)
								.addComponent(lblCliente))
						.addPreferredGap(ComponentPlacement.RELATED)
						.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
								.addComponent(comboBoxEmpresa, GroupLayout.PREFERRED_SIZE, 26, GroupLayout.PREFERRED_SIZE)
								.addComponent(comboBoxCliente, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addPreferredGap(ComponentPlacement.UNRELATED)
						.addGroup(gl_panel.createParallelGroup(Alignment.TRAILING)
								.addGroup(gl_panel.createSequentialGroup()
										.addComponent(lblPlaca)
										.addPreferredGap(ComponentPlacement.RELATED)
										.addComponent(textFieldPlaca, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
								.addComponent(btnVerificarReserva))
						.addPreferredGap(ComponentPlacement.UNRELATED)
						.addComponent(lblVaga)
						.addPreferredGap(ComponentPlacement.RELATED)
						.addComponent(comboBoxVagas, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addContainerGap())
				);
		panel.setLayout(gl_panel);
		getContentPane().setLayout(groupLayout);
	}
}
