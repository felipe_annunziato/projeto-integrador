package view;

import javax.swing.JInternalFrame;
import javax.swing.JOptionPane;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JPanel;
import javax.swing.border.TitledBorder;

import controller.AtendimentoController;
import dao.AtendimentoDAO;
import model.Atendimento;
import view.tables.AtendimentoTableModel;

import javax.swing.border.LineBorder;
import java.awt.Color;
import javax.swing.JTable;
import java.awt.Scrollbar;
import java.awt.ScrollPane;
import javax.swing.JScrollPane;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class FinalizaAtendimentoUI extends JInternalFrame{
	private JTable tableAtendimento;
	private AtendimentoController atendimentoController = new AtendimentoController();
	public FinalizaAtendimentoUI() {
		setClosable(true);
		setTitle("Atendimentos em andamento");

		JPanel panelAtendimento = new JPanel();

		JButton btnFinalizar = new JButton("Finalizar");
		btnFinalizar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Atendimento atendSelecionado = new AtendimentoTableModel(new AtendimentoController().listaAtendimentosAbertos()).get(tableAtendimento.getSelectedRow());
				atendimentoController.finalizaAtendimento(atendSelecionado);
				atendimentoController.atualizaStatusVagaFree(atendSelecionado.getVaga());
//				atendimentoController.calculaValor(atendSelecionado);
				JOptionPane.showMessageDialog(null, "Atendimento Finalizado!");
				tableAtendimento.setModel(new AtendimentoTableModel(new AtendimentoController().listaAtendimentosAbertos()));
			}
		});

		JButton btnCancelar = new JButton("Cancelar");
		btnCancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				dispose();
			}
		});
		GroupLayout groupLayout = new GroupLayout(getContentPane());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(panelAtendimento, GroupLayout.DEFAULT_SIZE, 416, Short.MAX_VALUE)
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(btnFinalizar)
							.addGap(18)
							.addComponent(btnCancelar)))
					.addContainerGap())
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addComponent(panelAtendimento, GroupLayout.DEFAULT_SIZE, 191, Short.MAX_VALUE)
					.addGap(18)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(btnFinalizar)
						.addComponent(btnCancelar))
					.addGap(22))
		);

		JScrollPane scrollPaneAtendimento = new JScrollPane();
		GroupLayout gl_panelAtendimento = new GroupLayout(panelAtendimento);
		gl_panelAtendimento.setHorizontalGroup(
				gl_panelAtendimento.createParallelGroup(Alignment.LEADING)
				.addComponent(scrollPaneAtendimento, GroupLayout.DEFAULT_SIZE, 667, Short.MAX_VALUE)
				);
		gl_panelAtendimento.setVerticalGroup(
				gl_panelAtendimento.createParallelGroup(Alignment.LEADING)
				.addComponent(scrollPaneAtendimento, GroupLayout.DEFAULT_SIZE, 269, Short.MAX_VALUE)
				);

		tableAtendimento = new JTable();
		tableAtendimento.setModel(new AtendimentoTableModel(new AtendimentoController().listaAtendimentosAbertos()));
		scrollPaneAtendimento.setViewportView(tableAtendimento);
		panelAtendimento.setLayout(gl_panelAtendimento);
		getContentPane().setLayout(groupLayout);
	}
}
