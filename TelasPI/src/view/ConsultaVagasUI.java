package view;

import java.awt.EventQueue;

import javax.swing.JInternalFrame;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import controller.ClienteController;
import controller.VagaController;
import model.Vaga;
import view.tables.VagaTableModel;

import javax.swing.JTextPane;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.JPanel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class ConsultaVagasUI extends JInternalFrame {
	private JTable tableVaga;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ConsultaVagasUI frame = new ConsultaVagasUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public ConsultaVagasUI() {
		setClosable(true);
		setTitle("AutoParking\n");
		setBounds(100, 100, 819, 715);
		
		JScrollPane scrollPane = new JScrollPane();
		
		JButton btnEditar = new JButton("Editar");
		btnEditar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Vaga vaga = new VagaTableModel(new VagaController().listarVagas()).get(tableVaga.getSelectedRow());
				EditarVagasUI cadVagas = new EditarVagasUI(vaga);
				cadVagas.setVisible(true);
				getParent().add(cadVagas, 0);
				
			}
		});
		GroupLayout groupLayout = new GroupLayout(getContentPane());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(22)
					.addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, 527, Short.MAX_VALUE)
					.addGap(26))
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(34)
					.addComponent(btnEditar)
					.addContainerGap(658, Short.MAX_VALUE))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.TRAILING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(8)
					.addComponent(btnEditar)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, 387, Short.MAX_VALUE))
		);
		
		
		tableVaga = new JTable();
		tableVaga.setModel(new VagaTableModel(new VagaController().listarVagas()));
		scrollPane.setViewportView(tableVaga);
		getContentPane().setLayout(groupLayout);

	}
}
