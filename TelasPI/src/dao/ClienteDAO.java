package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import model.UserCliente;
import util.ConnectionUtil;


public class ClienteDAO {
	
	private static Connection con =  ConnectionUtil.getConnection();
	
	public void salvar(UserCliente cliente) {
		
		try {
			String sql = "insert into Cliente (nome, username, cpf, telefone, email) values (?, ?, ?, ?, ?)";
			PreparedStatement stmt = con.prepareStatement(sql);
			stmt.setString(1, cliente.getNome());
			stmt.setString(2, cliente.getUsername());
			stmt.setString(3, cliente.getCpf());
			stmt.setString(4, cliente.getTelefone());
			stmt.setString(5, cliente.getEmail());
			
			stmt.execute();
			
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		
	}
	
	
	public void editar(UserCliente cliente) {
		
		try {
			String sql = "update Cliente set nome = ?, username = ?, cpf = ?, email = ?, telefone = ?  where idCliente = ?";
			PreparedStatement stmt = con.prepareStatement(sql);
			stmt.setString(1, cliente.getNome());
			stmt.setString(2, cliente.getUsername());
			stmt.setString(3, cliente.getCpf());
			stmt.setString(4, cliente.getEmail());
			stmt.setString(5, cliente.getTelefone());
			stmt.setInt(6, cliente.getId());
			stmt.executeUpdate();
			
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		
		
	}
	
	
	public List<UserCliente> listarClientes() {
		ArrayList<UserCliente> lista = new ArrayList<UserCliente>();
		try {
			String sql = "select * from Cliente";
			PreparedStatement stmt = con.prepareStatement(sql);
			ResultSet result = stmt.executeQuery();
			while(result.next()) {
				UserCliente cliente = new UserCliente();
				cliente.setId(result.getInt("idCliente"));
				cliente.setNome(result.getString("nome"));
				cliente.setCpf(result.getString("cpf"));
				cliente.setUsername(result.getString("username"));
				cliente.setEmail(result.getString("email"));
				cliente.setTelefone(result.getString("telefone"));
				lista.add(cliente);
				
			}
			
		} catch (Exception e) {
			System.out.println(e.getMessage());// TODO: handle exception
		}
		return lista;
	}
	
	public void excluir(Integer id) {
	try {
		String sql = "delete from Cliente where idCliente = ?";
		PreparedStatement stmt = con.prepareStatement(sql);
		stmt.setInt(1, id);
		stmt.execute();
		
	}catch (Exception e) {
		System.out.println(e.getMessage());
	}


	}
}