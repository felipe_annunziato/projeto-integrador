package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import model.Empresa;
import model.ReservaVaga;
import util.ConnectionUtil;

public class ReservaDAO {
	
	private Connection con = ConnectionUtil.getConnection();
	
	public List<ReservaVaga> listaReservas(Empresa empresa){
		ArrayList<ReservaVaga> lista = new ArrayList<ReservaVaga>();
		try {
			String sql = "select Vagas.idVagas, Vagas.status, Cliente.nome, Cliente.username, ReservaVaga.placa, ReservaVaga.hora, ReservaVaga.data, Empresa.razaoSocial from ReservaVaga "
					+ "join Vagas on Vagas.idVagas = ReservaVaga.idVagas"
					+ "join Cliente on Cliente.idCliente = ReservaVaga.idCliente "
					+ "join Empresa on Empresa.idEmpresa = ReservaVaga.idEmpresa"
					+ "where ReservaVaga.idEmpresa = ?";
			PreparedStatement stmt = con.prepareStatement(sql);
			stmt.setInt(1, empresa.getIdEmpresa());
			ResultSet result = stmt.executeQuery();
			while(result.next()) {
				ReservaVaga rv = new ReservaVaga();
				
				rv.setData(result.getDate("data"));
				rv.setHora(result.getTime("hora"));
				rv.setPlaca(result.getString("placa"));


				lista.add(rv);
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return lista;
	}

}
