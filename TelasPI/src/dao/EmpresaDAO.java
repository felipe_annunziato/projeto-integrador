package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import util.ConnectionUtil;
import model.Empresa;
import java.util.ArrayList;
import java.util.List;

public class EmpresaDAO {
	
private static Connection con = ConnectionUtil.getConnection();
	
	public void cadastrar(Empresa empresa) {
		try {
			String sql = "insert into Empresa (idGrupo, razaoSocial, endereco, telefone, cnpj, email) values (?, ?, ?, ?, ?, ?)";
			PreparedStatement stmt = con.prepareStatement(sql);
			stmt.setInt(1, 1);
			stmt.setString(2, empresa.getRazaoSocial());
			stmt.setString(3, empresa.getEndereco());
			stmt.setString(4, empresa.getTelefone());
			stmt.setString(5, empresa.getCnpj());
			stmt.setString(6, empresa.getEmail());
			stmt.execute();
		} catch(Exception e) {
			System.out.println(e.getMessage());
		}
	}
	
	public void editar(Empresa empresa) {
		String sql = "update Empresa set razaoSocial = ?, endereco = ?, telefone = ?, cnpj = ?, email = ? where idEmpresa = ? ";
		try {
			PreparedStatement stmt = con.prepareStatement(sql);
			stmt.setString(1, empresa.getRazaoSocial());
			stmt.setString(2, empresa.getEndereco());
			stmt.setString(3, empresa.getTelefone());
			stmt.setString(4, empresa.getCnpj());
			stmt.setString(5, empresa.getEmail());
			stmt.setInt(6, empresa.getIdEmpresa());
			stmt.executeUpdate();
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
	}
	
	public List<Empresa> buscarTodos() {
		ArrayList <Empresa> lista = new ArrayList<Empresa>();
		String sql = "select * from Empresa";
		try {
			PreparedStatement stmt = con.prepareStatement(sql);
			ResultSet result = stmt.executeQuery(sql);
			while(result.next()) {
				Empresa emp = new Empresa();
				emp.setIdEmpresa(result.getInt("idEmpresa"));
				emp.setRazaoSocial(result.getString("razaoSocial"));
				emp.setEndereco(result.getString("endereco"));
				emp.setTelefone(result.getString("telefone"));
				emp.setEmail(result.getString("email"));
				emp.setCnpj(result.getString("cnpj"));
				lista.add(emp);
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		
		return lista;
	}
	
	public void excluir(Integer id) {
		String sql = "delete from Empresa where idEmpresa = ?";
		try {
			PreparedStatement stmt = con.prepareStatement(sql);
			stmt.setInt(1, id);
			stmt.execute();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}
}
