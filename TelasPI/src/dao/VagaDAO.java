package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import com.mysql.cj.protocol.Resultset;

import model.Empresa;
import model.Vaga;
import util.ConnectionUtil;

public class VagaDAO {

	private static Connection con = ConnectionUtil.getConnection();

	public List<Vaga> listarVagas() {
		ArrayList <Vaga> lista = new ArrayList<Vaga>();
		try {
			String sql = "select Empresa.razaoSocial, Vagas.idVagas, Vagas.status, Vagas.valor_hora, Vagas.valor_diaria from Vagas inner join Empresa on Vagas.idEmpresa = Empresa.idEmpresa;";
			PreparedStatement stmt = con.prepareStatement(sql);
			ResultSet result = stmt.executeQuery();
			while(result.next()) {
				Vaga vaga = new Vaga();
				vaga.setIdVaga(result.getInt("idVagas"));
				vaga.setRazaoSocial(result.getString("razaoSocial"));
				vaga.setStatus(result.getString("status"));
				vaga.setValor_hora(result.getDouble("valor_hora"));
				vaga.setValor_diaria(result.getDouble("valor_diaria"));
				lista.add(vaga);
			}
		}catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return lista;
	}

	public void cadastraVagas(Vaga vaga){
		try {
			String sql = "insert into Vagas (idGrupo, idEmpresa, status, valor_hora, valor_diaria) values (?,?,?,?,?)";
			PreparedStatement stmt = con.prepareStatement(sql);
			stmt.setInt(1, vaga.getIdGrupo());
			stmt.setInt(2, vaga.getIdEmpresa());
			stmt.setString(3, "free");
			stmt.setDouble(4, vaga.getValor_hora());
			stmt.setDouble(5, vaga.getValor_diaria());
			stmt.execute();

		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	public List<Vaga> listarVagasLivres(Empresa empresa) {
		ArrayList<Vaga> lista = new ArrayList<Vaga>();
		try {
			String sql = "select * from Vagas where status = 'free' and idEmpresa = ?";
			PreparedStatement stmt = con.prepareStatement(sql);
			stmt.setInt(1, empresa.getIdEmpresa());
			ResultSet result = stmt.executeQuery();
			while(result.next()) {
				Vaga vaga = new Vaga();
				vaga.setIdVaga(result.getInt("idVagas"));
				lista.add(vaga);
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return lista;
	}
	
	
	public void editar(Vaga vaga) {
		try {
			String sql = "update Vagas set idEmpresa = ?, status = ?, valor_diaria = ?, valor_hora = ? where idVagas = ?";
			PreparedStatement stmt = con.prepareStatement(sql);
			stmt.setInt(1, vaga.getIdEmpresa());
			stmt.setString(2, vaga.getStatus());
			stmt.setDouble(3, vaga.getValor_diaria());
			stmt.setDouble(4, vaga.getValor_hora());
			stmt.setInt(5, vaga.getIdVaga());
			stmt.executeUpdate();
		}catch (Exception e) {
			System.out.println(e.getMessage());// TODO: handle exception
		}
	}

}
