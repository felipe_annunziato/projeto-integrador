package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import model.Atendimento;
import model.Empresa;
import model.UserCliente;
import model.Vaga;
import util.ConnectionUtil;

public class AtendimentoDAO {

	private Connection con = ConnectionUtil.getConnection();

	SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
	SimpleDateFormat sdh = new SimpleDateFormat("hh:mm");

	public List<Empresa> listaEmpresa(){
		ArrayList<Empresa> lista = new ArrayList<Empresa>();
		try {
			String sql = "select * from Empresa";
			PreparedStatement stmt = con.prepareStatement(sql);
			ResultSet result = stmt.executeQuery();
			while(result.next()) {
				Empresa empresa = new Empresa();
				empresa.setIdGrupo(result.getInt("idGrupo"));
				empresa.setIdEmpresa(result.getInt("idEmpresa"));
				empresa.setRazaoSocial(result.getString("razaoSocial"));
				lista.add(empresa);
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return lista;
	}

	public List<Atendimento> listaAtendimentosAbertos(){
		ArrayList<Atendimento> lista = new ArrayList<Atendimento>();
		try {
			String sql = "select Vagas.idVagas, Vagas.valor_hora, Vagas.valor_diaria, Atendimento.idAtendimento, Cliente.nome, "
					+ "Atendimento.placa,Atendimento.hora_Entrada, Atendimento.data_Entrada,Atendimento.hora_Saida, "
					+ "Atendimento.data_Saida, Atendimento.valor "
					+ "from Atendimento "
					+ "join Cliente on Cliente.idCliente = Atendimento.idCliente "
					+ "join Vagas on Vagas.idVagas = Atendimento.idVagas where hora_saida is null";
			PreparedStatement stmt = con.prepareStatement(sql);
			ResultSet result = stmt.executeQuery();
			while(result.next()) {
				Atendimento atendimento = new Atendimento();
				UserCliente u = new UserCliente();
				Vaga vaga = new Vaga();
				u.setNome(result.getString("nome"));
				vaga.setIdVaga(result.getInt("idVagas"));
				vaga.setValor_hora(result.getDouble("valor_hora"));
				vaga.setValor_diaria(result.getDouble("valor_diaria"));
				atendimento.setCliente(u);
				atendimento.setVaga(vaga);
				atendimento.setIdAtendimento(result.getInt("idAtendimento"));
				atendimento.setPlaca(result.getString("placa"));
				atendimento.setHoraEntrada(result.getString("hora_Entrada"));
				atendimento.setDataEntrada(result.getDate("data_Entrada"));
				atendimento.setHoraSaida(result.getString("hora_Saida"));
				atendimento.setDataSaida(result.getDate("data_Saida"));
				atendimento.setValor(result.getDouble("valor"));
				lista.add(atendimento);
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return lista;
	}

	public List<Atendimento> listaAtendimentosFechados(){
		ArrayList<Atendimento> lista = new ArrayList<Atendimento>();
		try {
			String sql = "select Vagas.idVagas, Atendimento.idAtendimento, Cliente.nome, Atendimento.placa,Atendimento.hora_Entrada, "
					+ "Atendimento.data_Entrada,Atendimento.hora_Saida, Atendimento.data_Saida, Atendimento.valor from Atendimento "
					+ "join Cliente on Cliente.idCliente = Atendimento.idCliente join Vagas on Vagas.idVagas = Atendimento.idVagas "
					+ "where hora_Saida is not null";
			PreparedStatement stmt = con.prepareStatement(sql);
			ResultSet result = stmt.executeQuery();
			while(result.next()) {
				Atendimento atendimento = new Atendimento();
				UserCliente u = new UserCliente();
				Vaga vaga = new Vaga();
				u.setNome(result.getString("nome"));
				vaga.setIdVaga(result.getInt("idVagas"));
				atendimento.setCliente(u);
				atendimento.setVaga(vaga);
				atendimento.setIdAtendimento(result.getInt("idAtendimento"));
				atendimento.setPlaca(result.getString("placa"));
				atendimento.setHoraEntrada(result.getString("hora_Entrada"));
				atendimento.setDataEntrada(result.getDate("data_Entrada"));
				atendimento.setHoraSaida(result.getString("hora_Saida"));
				atendimento.setDataSaida(result.getDate("data_Saida"));
				atendimento.setValor(result.getDouble("valor"));
				lista.add(atendimento);
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return lista;
	}

	public void iniciaAtendimento(Empresa empresa, UserCliente cliente, Atendimento atendimento) {
		Date now = new Date();
		Calendar c = Calendar.getInstance();
		c.setTime(now);
		int hora = c.get(Calendar.HOUR_OF_DAY);
		int min = c.get(Calendar.MINUTE);
		String time = hora+":"+min;
		int dia = c.get(Calendar.DAY_OF_MONTH);
		int mes = c.get(Calendar.MONTH);
		int ano = c.get(Calendar.YEAR);
		String data = ano+"-"+mes+"-"+dia;

		try {
			String sql = "insert into Atendimento (idGrupo, idEmpresa, placa, idCliente, data_Entrada, hora_Entrada, idVagas) values (?,?,?,?,?,?,?)";
			PreparedStatement stmt = con.prepareStatement(sql);
			stmt.setInt(1, empresa.getIdGrupo());
			stmt.setInt(2, empresa.getIdEmpresa());
			stmt.setString(3, atendimento.getPlaca());
			stmt.setInt(4, cliente.getId());
			stmt.setString(5, data);
			stmt.setString(6, time);
			stmt.setInt(7, atendimento.getVaga().getIdVaga());
			stmt.execute();

		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	public void finalizaAtendimento(Atendimento atendimento) {
		Date now = new Date();
		Calendar c = Calendar.getInstance();
		c.setTime(now);
		int hora = c.get(Calendar.HOUR_OF_DAY);
		int min = c.get(Calendar.MINUTE);
		String time = hora+":"+min;
		int dia = c.get(Calendar.DAY_OF_MONTH);
		int mes = c.get(Calendar.MONTH);
		int ano = c.get(Calendar.YEAR);
		String data = ano+"-"+mes+"-"+dia;

		try {
			String sql = "update Atendimento set data_Saida = ?, hora_Saida = ?, valor = ? where idAtendimento = ?";
			PreparedStatement stmt = con.prepareStatement(sql);
			stmt.setString(1, data);
			stmt.setString(2, time);
			stmt.setDouble(3, atendimento.getValor());
			stmt.setInt(4, atendimento.getIdAtendimento());
			stmt.execute();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

//	public void calculaValor(Atendimento atendimento) {
//		Date now = new Date();
//		Calendar c = Calendar.getInstance();
//		c.setTime(now);
//		int hora = c.get(Calendar.HOUR_OF_DAY);
//		int min = c.get(Calendar.MINUTE);
//		String time = hora+":"+min;
//		int dia = c.get(Calendar.DAY_OF_MONTH);
//		int mes = c.get(Calendar.MONTH);
//		int ano = c.get(Calendar.YEAR);
//		String data = ano+"-"+mes+"-"+dia;
//		Calendar c2 = Calendar.getInstance();
//		c2.setTime(atendimento.getDataEntrada());
//		int horaEntrada = c2.get(Calendar.HOUR_OF_DAY);
//		Calendar c3 = Calendar.getInstance();
//		System.out.println(atendimento.getHoraEntrada());
		
		
//		int h = hora - horaEntrada;
//		Double valor = 0.0;
//		if(d!=0) {
//			valor += d*atendimento.getVaga().getValor_diaria();
//		}else {
//			valor += h*atendimento.getVaga().getValor_hora();
//		}
//
//				try {
//					String sql = "update Atendimento set valor = ? where idAtendimento = ?";
//					PreparedStatement stmt = con.prepareStatement(sql);
//		
//					stmt.setInt(2, atendimento.getVaga().getIdVaga());
//		
//				} catch (Exception e) {
//					System.out.println(e.getMessage());
//				}

//	}

	public void atualizaStatusVagaFree(Vaga vaga) {
		try {
			String sql = "update Vagas set status = 'free' where idVagas = ?";
			PreparedStatement stmt = con.prepareStatement(sql);
			stmt.setInt(1, vaga.getIdVaga());
			stmt.execute();
		} catch (Exception e) {
			System.out.println(e.getMessage()); 
		}
	}

	public void atualizaStatusVagaBusy(Vaga vaga) {
		try {
			String sql = "update Vagas set status = 'busy' where idVagas = ?";
			PreparedStatement stmt = con.prepareStatement(sql);
			stmt.setInt(1, vaga.getIdVaga());
			stmt.execute();
		} catch (Exception e) {
			System.out.println(e.getMessage()); 
		}
	}


}
