package model;

public class Vaga extends Empresa{

	private Integer idVaga;
	private String status;
	private Double valor_hora;
	private Double valor_diaria;

	public Double getValor_hora() {
		return valor_hora;
	}
	public void setValor_hora(Double valor_hora) {
		this.valor_hora = valor_hora;
	}
	public Double getValor_diaria() {
		return valor_diaria;
	}
	public void setValor_diaria(Double valor_diaria) {
		this.valor_diaria = valor_diaria;
	}
	public Integer getIdVaga() {
		return idVaga;
	}
	public void setIdVaga(Integer idVaga) {
		this.idVaga = idVaga;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}

	public String toString() {
		return String.valueOf(idVaga);
	}

}	