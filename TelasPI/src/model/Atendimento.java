package model;

import java.sql.Time;
import java.util.Date;

public class Atendimento {
	
	private Vaga vaga;
//	private ReservaVaga reservaVaga;
	private String horaEntrada;
	private Date dataEntrada;
	private String horaSaida;
	private Date dataSaida;
	private String placa;
	private Double valor;
	private UserCliente cliente;
	private Integer idAtendimento;
	
	public String getPlaca() {
		return placa;
	}
	public void setPlaca(String placa) {
		this.placa = placa;
	}
	public Vaga getVaga() {
		return vaga;
	}
	public void setVaga(Vaga vaga) {
		this.vaga = vaga;
	}
//	public ReservaVaga getReservaVaga() {
//		return reservaVaga;
//	}
//	public void setReservaVaga(ReservaVaga reservaVaga) {
//		this.reservaVaga = reservaVaga;
//	}
	public Date getDataEntrada() {
		return dataEntrada;
	}
	public void setDataEntrada(Date dataEntrada) {
		this.dataEntrada = dataEntrada;
	}
	public Date getDataSaida() {
		return dataSaida;
	}
	public void setDataSaida(Date dataSaida) {
		this.dataSaida = dataSaida;
	}
	public String getHoraEntrada() {
		return horaEntrada;
	}
	public void setHoraEntrada(String horaEntrada) {
		this.horaEntrada = horaEntrada;
	}
	public String getHoraSaida() {
		return horaSaida;
	}
	public void setHoraSaida(String horaSaida) {
		this.horaSaida = horaSaida;
	}
	public Double getValor() {
		return valor;
	}
	public void setValor(Double valor) {
		this.valor = valor;
	}
	public UserCliente getCliente() {
		return cliente;
	}
	public void setCliente(UserCliente cliente) {
		this.cliente = cliente;
	}
	public Integer getIdAtendimento() {
		return idAtendimento;
	}
	public void setIdAtendimento(Integer idAtendimento) {
		this.idAtendimento = idAtendimento;
	}

}
