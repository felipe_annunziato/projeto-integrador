package model;

import java.util.Date;

public class ReservaVaga {
	
	private Vaga vaga;
	private Empresa empresa;
	private Date data;
	private Date hora;
	private String placa;
	private UserCliente userCliente;	
		
	
	public Vaga getVaga() {
		return vaga;
	}
	public void setVaga(Vaga vaga) {
		this.vaga = vaga;
	}
	public String getPlaca() {
		return placa;
	}
	public void setPlaca(String placa) {
		this.placa = placa;
	}
	public UserCliente getUserCliente() {
		return userCliente;
	}
	public void setUserCliente(UserCliente userCliente) {
		this.userCliente = userCliente;
	}
	public void reserva() {}
	public Empresa getEmpresa() {
		return empresa;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	public Date getData() {
		return data;
	}
	public void setData(Date data) {
		this.data = data;
	}
	public Date getHora() {
		return hora;
	}
	public void setHora(Date hora) {
		this.hora = hora;
	};
	

}
 